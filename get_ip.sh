#!/bin/sh

newip=$(curl -s http://whatismyip.akamai.com/)

oldip=$(cat ip.txt)

if [ "$newip" == "$oldip" ]; then 
    echo "No change to IP address"
else 
    echo "IP changed - updating..."
    cp /dev/null ./ip.txt
    echo "$newip" >> ./ip.txt
fi

echo "testicles"
